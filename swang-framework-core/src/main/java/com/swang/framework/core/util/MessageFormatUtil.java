package com.swang.framework.core.util;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.text.MessageFormat;
import java.util.List;

/**
 * @Description:
 * @ClassName: MessageFromatUtil.java
 * @Package: com.yuntai.med.core.util
 * @Author: wangxiaoning@hsyuntai.com
 * @Date: 2016年1月21日 下午4:44:15
 * @Copyright: 版权归 恒生芸泰网络 所有
 */
public class MessageFormatUtil {
	
	private MessageFormatUtil(){
		
	}
	
	public static String format(String message, Object... params) {
		if(message == null) {
			return "";
		}
		int size =ArrayUtils.getLength(params);
		if (size > 0) {
			return  MessageFormat.format(message, params);
		}
		return message;
	}

	public static String format(final String message,final List<?> params) {
		int size = CollectionUtils.size(params);
		return format(message, (Object[]) params.toArray(new Object[size]));
	}

}
