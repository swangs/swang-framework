package com.swang.framework.core.util;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;

/**
 * @ClassName: EnumUtil
 * @Description: 枚举工具类
 * @author: swang@swang.com
 * @date: 2017年3月16日 下午9:45:51
 */
public class EnumUtil {

    public static boolean contains(String value, Class<?> clazz, String fieldName) throws Exception {
        if (StringUtils.isNotEmpty(value) && clazz.isEnum() && null != clazz.getEnumConstants()) {
            for (Object obj : clazz.getEnumConstants()) {
                Field field = clazz.getDeclaredField(fieldName);
                if (null != field) {
                    field.setAccessible(true);
                    Object codeValue = field.get(obj);
                    if (value.equals(codeValue)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
