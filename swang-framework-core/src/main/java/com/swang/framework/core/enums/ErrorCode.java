package com.swang.framework.core.enums;

public enum ErrorCode {
    /*
     * 执行成功：0
     */
    SUCCESS("0", "执行成功"),
    /*
     * 通用错误：-1 ~ -99
     */
    COMMON_ERROR("-10", "执行错误"),
    SERVICE_ERROR("-20", "业务服务错误"),
    ILLEGAL_ACCESS("-30", "非法访问"),
    /* 
     * 参数错误：-100 ~ -999
     */
    PARAM_ERROR("-100", "参数错误"),
    PARAM_REQUIRED("-101", "缺少参数"),
    PARAM_TOO_LONG("-102", "参数超过长度限制"),
    PARAM_MALFORMED("-103", "格式错误"),
    PARAM_NOT_AN_OPTION("-104", "不是可选值"),
    
    PICTURE_NOT_SUPPORT("-105", "图片类型不支持"),
    FILE_SAVE_ERROR("-106", "文件保存失败"),
    
    DICT_ERROR("-107", "字典项为空"),
    
  
    NOT_ERROR("-999999", "不是错误"),
    ;
	/** 执行成功 */
	public static final String CODE_SUCCESS = "0";
	/** 不是错误，用于一些默认的处理 */
	public static final String CODE_NOT_ERROR = "-999999";
    private String code;
    private String description;
    
    private ErrorCode(String code, String description) {
        this.code = code;
        this.description = description;
    }
    
    private ErrorCode(String code, String description, String solution) {
        this.code = code;
        this.description = description;
    }
    
    public String getCode() {
        return code;
    }
    
    public String getDescription() {
        return description;
    }
}
