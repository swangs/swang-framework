package com.swang.framework.core.util;

import okhttp3.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName: HttpUtil
 * @Description: TODO
 * @author: swang@swang.com
 * @date: 2017年3月16日 下午9:45:51
 */
public class OkHttpUtil {
    private static Logger log = Logger.getLogger(OkHttpUtil.class);

    public final static long CONNECT_TIMEOUT = 60L; //设置连接超时时间
    public final static long READ_TIMEOUT = 100L; //设置读取超时时间
    public final static long WRITE_TIMEOUT = 60L; //设置写的超时时间
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private static OkHttpClient client = new OkHttpClient().newBuilder()
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .build();

    public static String post(String url, String json) throws IOException {
        log.debug("====>http请求-->请求地址:" + url);
        String resultJson = "";
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            resultJson = response.body().string();
        } else {
            throw new IOException("Unexpected code " + response);
        }
        log.debug("====>http请求-->返回字符串:" + resultJson);
        return resultJson;
    }

    public static String get(String url) throws IOException {
        log.debug("====>http请求-->请求地址:" + url);
        String resultJson = "";

        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            resultJson = response.body().string();
        } else {
            throw new IOException("Unexpected code " + response);
        }
        log.debug("====>http请求-->返回字符串:" + resultJson);
        return resultJson;
    }

    public static void main(String[] args) throws IOException {

//创建一个Request
        final Request request = new Request.Builder()
                .url("https://github.com/hongyangAndroid")
                .build();
//new call
        Response response = client.newCall(request).execute();

        if (response.isSuccessful()) {
            System.out.println(response.body().string());
        }
//        Call call = mOkHttpClient.newCall(request);
////请求加入调度
//        call.enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                String htmlStr =  response.body().string();
//                System.out.println("htmlStr->" + htmlStr);
//            }
//
//        });
    }

}
