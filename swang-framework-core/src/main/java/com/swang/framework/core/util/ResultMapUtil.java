package com.swang.framework.core.util;


import com.swang.framework.core.base.ResultObject;
import com.swang.framework.core.constant.Fields;
import com.swang.framework.core.enums.ErrorCode;

import java.util.HashMap;
import java.util.Map;

public class ResultMapUtil {
    
    public static Map<String, Object> createSuccessResult() {
        Map<String, Object> result = new HashMap<String, Object>();
        return makeSuccessResult(result);
    }
    
    public static Map<String, Object> createSuccessResult(String errorInfo) {
        return buildErrorResult(ErrorCode.SUCCESS.getCode(), errorInfo);
    }
    
    public static Map<String, Object> makeSuccessResult(Map<String, Object> result) {
        result.put(Fields.ERROR_NO, ErrorCode.SUCCESS.getCode());
        return result;
    }
    
    public static Map<String, Object> makeSuccessResult(Map<String, Object> result, String errorInfo) {
        return setErrorResult(result, ErrorCode.SUCCESS.getCode(), errorInfo);
    }
    
    public static Map<String, Object> genCommonErrorResult() {
        return genErrorResult(ErrorCode.COMMON_ERROR);
    }
    
    public static Map<String, Object> genCommonErrorResult(String errorInfo) {
        return buildErrorResult(ErrorCode.COMMON_ERROR.getCode(), errorInfo);
    }
    
    public static Map<String, Object> genErrorResult(ErrorCode errorCode) {
        return buildErrorResult(errorCode.getCode(), errorCode.getDescription());
    }
    
    public static Map<String, Object> buildErrorResult(String errorNo, String errorInfo) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put(Fields.ERROR_NO, errorNo);
        result.put(Fields.ERROR_INFO, errorInfo);
        return result;
    }
    
    public static Map<String, Object> genErrorResultWithExtraInfo(ErrorCode errorCode, String extraInfo) {
        return buildErrorResult(errorCode.getCode(), buildErrorInfoWithExtraInfo(errorCode, extraInfo));
    }
    
    public static Map<String, Object> setCommonErrorResult(Map<String, Object> result) {
        return setErrorResult(result, ErrorCode.COMMON_ERROR);
    }
    
    public static Map<String, Object> setErrorResult(Map<String, Object> result, ErrorCode errorCode) {
        return setErrorResult(result, errorCode.getCode(), errorCode.getDescription());
    }
    
	public static Map<String, Object> setErrorResult(Map<String, Object> result, String errorNo, String errorInfo) {
		result.put(Fields.ERROR_NO, errorNo);
		result.put(Fields.ERROR_INFO, errorInfo);
		return result;
	}
    
    public static Map<String, Object> setErrorResultWithExtraInfo(Map<String, Object> result, ErrorCode errorCode,
            String extraInfo) {
        return setErrorResult(result, errorCode.getCode(), buildErrorInfoWithExtraInfo(errorCode, extraInfo));
    }
    
    public static String buildErrorInfoWithExtraInfo(ErrorCode errorCode, String extraInfo) {
        return new StringBuilder().append(errorCode.getDescription()).append('[').append(extraInfo).append(']')
                .toString();
    }
    
    public static boolean isSuccess(Map<String, Object> result) {
        return null != result
                && ErrorCode.CODE_SUCCESS.equals( MapUtil.getString(result, Fields.ERROR_NO,
                        ErrorCode.CODE_NOT_ERROR));
    }
    
    public static String getErrorNo(Map<String, Object> result) {
        if(result != null) {
            Object error_no = result.get(Fields.ERROR_NO);
            if(error_no instanceof String) {
                return error_no.toString();
            }
        }
        return ErrorCode.CODE_NOT_ERROR;
    }
    public static String getErrorInfo(Map<String, Object> result) {
        if(result != null) {
            Object errorInfo = result.get(Fields.ERROR_INFO);
            if(errorInfo instanceof String) {
                return errorInfo.toString();
            }
        }
        return "";
    }
    public static boolean isErrorOf(Map<String, Object> result, ErrorCode errorCode) {
        if (null != result) {
            Object error_no = result.get(Fields.ERROR_NO);
            if (null != error_no) {
                return String.valueOf(errorCode.getCode()).equals(String.valueOf(error_no));
            }
        }
        return false;
    }
    
    public static boolean isErrorOf(Map<String, Object> result, int errorNo) {
        if (null != result) {
            Object error_no = result.get(Fields.ERROR_NO);
            if (null != error_no) {
                return String.valueOf(errorNo).equals(String.valueOf(error_no));
            }
        }
        return false;
    }


    public static final ResultObject createResultObject(String errorNo, String errorInf) {
       return ResultObject.createFail(errorNo, errorInf);
    }
}
