package com.swang.framework.core.base;


import com.swang.framework.core.util.MessageFormatUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Description:
 * @ClassName: ErrorMessage.java
 * @Package: com.yuntai.med.core.base
 * @Author: wangxiaoning@hsyuntai.com
 * @Date: 2016年1月21日 下午2:34:32
 * @Copyright: 版权归 恒生芸泰网络 所有
 */
public class ErrorMessage implements Serializable {
	private static final long serialVersionUID = -954571847283392996L;

	private int errorNo;
	private String message;
	private List<?> params;

	public ErrorMessage() {
		errorNo = 0;
		message = "";
	}

	public static final ErrorMessage create(int errorNo, String message, Object... params) {
		return new ErrorMessage(errorNo, message, params);
	}

	public static final ErrorMessage create(int errorNo, String message) {
		return new ErrorMessage(errorNo, message, new Object[0]);
	}

	private ErrorMessage(int errorNo, String message, Object... params) {
		super();
		this.errorNo = errorNo;
		this.message = message;
		if (params != null) {
			this.params = Arrays.asList(params);
		} else {
			this.params = new ArrayList<Object>();
		}
	}

	public int getErrorNo() {
		return errorNo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/** 此信息通过后面参数组装好的信息 */
	public String resolveMessage() {
		 return MessageFormatUtil.format(message, params);
	}
	/** 此信息通过后面参数组装好的信息 */
	public String resolveMessage(Object... params) {
		 return MessageFormatUtil.format(message, params);
	}
	public List<?> getParams() {
		return params;
	}
	
	public static void main(String[] args) {
		System.out.println("=================================");
		ErrorMessage em = ErrorMessage.create(60001, "字段{0}值{1}无效！","KEY","NULL");
		System.out.println(em.resolveMessage());
		ErrorMessage em1= ErrorMessage.create(60001, "字段{0}值{1} {2}无效！","KEY","NULL");
		System.out.println(em1.resolveMessage());
		ErrorMessage em2= ErrorMessage.create(60001, "字段{0}值无效！","KEY","NULL");
		System.out.println(em2.resolveMessage());
		ErrorMessage em3= ErrorMessage.create(60001, "字段{0}值无效！",em);
		System.out.println(em3.resolveMessage());
		ErrorMessage em4= ErrorMessage.create(60001, null,em);
		System.out.println(em4.resolveMessage());
		System.out.println("=================================");
	}
}
