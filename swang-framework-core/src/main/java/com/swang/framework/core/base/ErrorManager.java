package com.swang.framework.core.base;

import com.swang.framework.core.constant.BaseErrorMessage;
import com.swang.framework.core.constant.BaseErrorNo;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @ClassName: ErrorManager.java
 * @Package: com.yuntai.med.core.base
 * @Author: wangxiaoning@hsyuntai.com
 * @Date: 2016年1月21日 下午5:06:07
 * @Copyright: 版权归 恒生芸泰网络 所有
 */
public class ErrorManager {
	public static final Map<Integer, ErrorMessage> ERROR_MAP = new HashMap<Integer, ErrorMessage>();
	static {
		try {
			addMessage(BaseErrorNo.ERROR_GENERAL_1001, BaseErrorMessage.MESSAGE_GENERAL_1001);
			addMessage(BaseErrorNo.ERROR_GENERAL_1002, BaseErrorMessage.MESSAGE_GENERAL_1002);
			addMessage(BaseErrorNo.ERROR_GENERAL_1003, BaseErrorMessage.MESSAGE_GENERAL_1003);
			addMessage(BaseErrorNo.ERROR_GENERAL_1004, BaseErrorMessage.MESSAGE_GENERAL_1004);
			addMessage(BaseErrorNo.ERROR_GENERAL_1005, BaseErrorMessage.MESSAGE_GENERAL_1005);
			addMessage(BaseErrorNo.ERROR_GENERAL_1006, BaseErrorMessage.MESSAGE_GENERAL_1006);
			addMessage(BaseErrorNo.ERROR_GENERAL_1007, BaseErrorMessage.MESSAGE_GENERAL_1007);
			addMessage(BaseErrorNo.ERROR_AUTH_3003, BaseErrorMessage.MESSAGE_AUTH_3003);
			addMessage(BaseErrorNo.ERROR_AUTH_3005, BaseErrorMessage.MESSAGE_AUTH_3005);
			addMessage(BaseErrorNo.ERROR_AUTH_3006, BaseErrorMessage.MESSAGE_AUTH_3006);
			addMessage(BaseErrorNo.ERROR_AUTH_3007, BaseErrorMessage.MESSAGE_AUTH_3007);
			addMessage(BaseErrorNo.ERROR_AUTH_3009, BaseErrorMessage.MESSAGE_AUTH_3009);
			addMessage(BaseErrorNo.ERROR_AUTH_3010, BaseErrorMessage.MESSAGE_AUTH_3010);
			addMessage(BaseErrorNo.ERROR_AUTH_3011, BaseErrorMessage.MESSAGE_AUTH_3011);
			addMessage(BaseErrorNo.ERROR_AUTH_3014, BaseErrorMessage.MESSAGE_AUTH_3014);
			addMessage(BaseErrorNo.ERROR_AUTH_3015, BaseErrorMessage.MESSAGE_AUTH_3015);
			addMessage(BaseErrorNo.ERROR_AUTH_3016, BaseErrorMessage.MESSAGE_AUTH_3016);
			addMessage(BaseErrorNo.ERROR_AUTH_3017, BaseErrorMessage.MESSAGE_AUTH_3017);
			addMessage(BaseErrorNo.ERROR_NET_5001, BaseErrorMessage.MESSAGE_NET_5001);
			addMessage(BaseErrorNo.ERROR_NET_5002, BaseErrorMessage.MESSAGE_NET_5002);
			addMessage(BaseErrorNo.ERROR_NET_5003, BaseErrorMessage.MESSAGE_NET_5003);
			addMessage(BaseErrorNo.ERROR_NET_5004, BaseErrorMessage.MESSAGE_NET_5004);
			addMessage(BaseErrorNo.ERROR_NET_5005, BaseErrorMessage.MESSAGE_NET_5005);
			
			addMessage(BaseErrorNo.ERROR_SYS_6001, BaseErrorMessage.MESSAGE_SYS_6001);
			addMessage(BaseErrorNo.ERROR_SYS_6002, BaseErrorMessage.MESSAGE_SYS_6002);
			addMessage(BaseErrorNo.ERROR_SYS_6003, BaseErrorMessage.MESSAGE_SYS_6003);
			addMessage(BaseErrorNo.ERROR_SYS_6004, BaseErrorMessage.MESSAGE_SYS_6004);
			addMessage(BaseErrorNo.ERROR_SYS_6005, BaseErrorMessage.MESSAGE_SYS_6005);
			addMessage(BaseErrorNo.ERROR_SYS_6006, BaseErrorMessage.MESSAGE_SYS_6006);
			addMessage(BaseErrorNo.ERROR_SYS_6007, BaseErrorMessage.MESSAGE_SYS_6007);
			
			addMessage(BaseErrorNo.ERROR_PARAM_7001, BaseErrorMessage.MESSAGE_PARAM_7001);
			addMessage(BaseErrorNo.ERROR_PARAM_7002, BaseErrorMessage.MESSAGE_PARAM_7002);
			addMessage(BaseErrorNo.ERROR_PARAM_7003, BaseErrorMessage.MESSAGE_PARAM_7003);
			addMessage(BaseErrorNo.ERROR_PARAM_7004, BaseErrorMessage.MESSAGE_PARAM_7004);
			addMessage(BaseErrorNo.ERROR_PARAM_7005, BaseErrorMessage.MESSAGE_PARAM_7005);
			addMessage(BaseErrorNo.ERROR_PARAM_7006, BaseErrorMessage.MESSAGE_PARAM_7006);
			addMessage(BaseErrorNo.ERROR_PARAM_7007, BaseErrorMessage.MESSAGE_PARAM_7007);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static boolean addMessage(Integer errorNo, String message) {
		return addMessage(ErrorMessage.create(errorNo, message));
	}
	
	public static boolean addMessage(ErrorMessage errorMessage) {
		if (errorMessage == null) {
			return false;
		} else {
			ERROR_MAP.put(errorMessage.getErrorNo(), errorMessage);
		}
		return true;
	}

	public static boolean addMessage(Integer errorNo, String message, Object... params) {
		return addMessage(ErrorMessage.create(errorNo, message, params));
	}

	public static String getMessage(Integer errorNo) {
		ErrorMessage em = ERROR_MAP.get(errorNo);
		if (em != null) {
			return em.resolveMessage();
		}
		return "";
	}
	public static String getMessage(Integer errorNo,Object... params) {
		ErrorMessage em = ERROR_MAP.get(errorNo);
		if (em != null) {
			return em.resolveMessage(params);
		}
		return "";
	}
	public static ErrorMessage getErrorMessage(Integer errorNo) {
		return ERROR_MAP.get(errorNo);
	}

	public static void main(String[] args) {
	}

}
