package com.swang.framework.core.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Set;

/**
 * Created by swang@swang.com on 2017/4/15.
 */
@Repository
public class LogInfoDaoImpl implements  LogInfoDao {

    public static final Logger logger = LoggerFactory.getLogger(LogInfoDaoImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void test() {
        Set<String> colls = this.mongoTemplate.getCollectionNames();
        for (String coll : colls) {
            logger.info("CollectionName=" + coll);
        }
        DB db = this.mongoTemplate.getDb();
        logger.info("db=" + db.toString());
    }

    @Override
    public void createCollection() {
        if (!this.mongoTemplate.collectionExists(LogInfo.class)) {
            this.mongoTemplate.createCollection(LogInfo.class);
        }
    }

    @Override
    public List<LogInfo> findList(int skip, int limit) {
        Query query = new Query();
        query.with(new Sort(new Order(Direction.ASC, "_id")));
        query.skip(skip).limit(limit);
        return this.mongoTemplate.find(query, LogInfo.class);
    }

    @Override
    public LogInfo findOne(Long id) {
        Query query = new Query();
        query.addCriteria(new Criteria("_id").is(id));
        return this.mongoTemplate.findOne(query, LogInfo.class);
    }

    @Override
    public LogInfo findOneByBizIdType(Long bizId, String bizModule, Integer bizType) {
        return null;
    }

    @Override
    public void insert(LogInfo logInfo) {
        // 设置需要插入到数据库的文档对象
//        DBObject object = new BasicDBObject();
//        object.put("id", logInfo.getId());
//        object.put("biz_id", logInfo.getBizId());
//        object.put("biz_type", logInfo.getBizType());
//        object.put("biz_module", logInfo.getBizModule());
//        object.put("trace_id", logInfo.getTraceId());
//        object.put("log_info", logInfo.getLogInfo());
//        object.put("log_data", logInfo.getLogData());
//        object.put("log_level", logInfo.getLogLevel());
//        object.put("create_time", logInfo.getCreateTime());
//        mongoTemplate.insert(object, "sys_biz_log");
        getMongoTemplate().insert(logInfo);
    }

    /**
     * @return the mongoTemplate
     */
    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    /**
     * @param mongoTemplate the mongoTemplate to set
     */
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
