package com.swang.framework.core.base;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @Description: 所有的Bean可继承此类
 * @ClassName: BaseForm.java
 * @Package:   com.yuntai.med.core.base
 * @Author:    wangxiaoning@hsyuntai.com
 * @Date:      2016年1月18日 下午8:03:32
 * @Copyright: 版权归  恒生芸泰网络  所有
 */
public class BaseBean implements Serializable { 
	private static final long serialVersionUID = 5451629273107059348L;

	@Override
	public String toString() {
		 return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE); 
	} 

}
