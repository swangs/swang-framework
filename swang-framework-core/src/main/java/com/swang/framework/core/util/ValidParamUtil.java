package com.swang.framework.core.util;


import com.swang.framework.core.constant.BaseErrorMessage;
import com.swang.framework.core.constant.BaseErrorNo;
import com.swang.framework.core.enums.ValidType;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 
 * @ClassName: ValidParamUtil.java
 * @Package:   com.yuntai.med.core.util
 * @Author:    wangxiaoning@hsyuntai.com
 * @Date:      2016年1月27日 下午4:32:01
 * @Copyright: 版权归  恒生芸泰网络  所有
 */
public class ValidParamUtil {
	public static void valid(Map<String,Object> params, FunctionParamValid fpv) {
		String key = null;
		if(fpv == null) {
			return;
		} else if(params == null) {
			for(Map.Entry<String, ValidType> entry:fpv.getValidParams().entrySet()){
				key = entry.getKey();
				valid(key, null,FunctionParamValid.getValidType(fpv, key));
			}
		} else {
			for(Map.Entry<String, ValidType> entry:fpv.getValidParams().entrySet()){
				key = entry.getKey();
				valid(key, params.get(key),FunctionParamValid.getValidType(fpv, key));
			}
		}
	}

	
	public static void valid(String paramName, Object paramValue, ValidType vt){
		if(vt == null) {
			return;
		}
 		switch(vt){
 			case NOT_NULL:
 				Assert.notNull(paramValue, BaseErrorNo.ERROR_PARAM_7001, MessageFormatUtil.format(BaseErrorMessage.MESSAGE_PARAM_7001, paramName));
 				break;
 			case NOT_EMPTY:
 				Assert.notEmpty(ValueUtil.getString(paramValue), BaseErrorNo.ERROR_PARAM_7002, MessageFormatUtil.format(BaseErrorMessage.MESSAGE_PARAM_7002, paramName));
 				break;
 			case NOT_INT:
 				Assert.isTrue(ValueUtil.getInt(paramValue,Integer.MAX_VALUE) != Integer.MAX_VALUE, BaseErrorNo.ERROR_PARAM_7003, MessageFormatUtil.format(BaseErrorMessage.MESSAGE_PARAM_7003, paramName, paramValue));
 				break;
			case NOT_LONG:
 				Assert.isTrue(ValueUtil.getLong(paramValue,Long.MAX_VALUE) != Long.MAX_VALUE, BaseErrorNo.ERROR_PARAM_7003, MessageFormatUtil.format(BaseErrorMessage.MESSAGE_PARAM_7003, paramName, paramValue));
 				break;
			case NOT_FLOAT:
			case NOT_DOUBLE:
 				Assert.isTrue(ValueUtil.getDouble(paramValue,Double.MAX_VALUE) != Double.MAX_VALUE, BaseErrorNo.ERROR_PARAM_7004, MessageFormatUtil.format(BaseErrorMessage.MESSAGE_PARAM_7004, paramName, paramValue));
 				break;
 			case NOT_BOOLEAN:
 				Assert.isTrue((paramValue instanceof Boolean || "true".equals(String.valueOf(paramValue)) || "false".equals(String.valueOf(paramValue))),
 						BaseErrorNo.ERROR_PARAM_7005, MessageFormatUtil.format(BaseErrorMessage.MESSAGE_PARAM_7005, paramName, paramValue));
 				break;
 			case NOT_STR_DATE:
 				Assert.notNull(ValueUtil.getDate(paramValue), BaseErrorNo.ERROR_PARAM_7006, MessageFormatUtil.format(BaseErrorMessage.MESSAGE_PARAM_7006, paramName,paramValue));
 				break;
 			case NOT_STR_DATETIME:
 				Assert.notNull(ValueUtil.getDateTime(paramValue), BaseErrorNo.ERROR_PARAM_7007, MessageFormatUtil.format(BaseErrorMessage.MESSAGE_PARAM_7007, paramName,paramValue));
 				break;
 			default:
 				break;
 		}
	}
	public static void main(String[] args) {
		
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("orderId", "2016-08-07 14:12:27");
		params.put("hosId", "600001");
		params.put("usName", "1");
		params.put("usId", null);
		
		FunctionParamValid fpv = FunctionParamValid.create("hosId", ValidType.NOT_LONG).addValid("usName",ValidType.NOT_EMPTY).addValid("usId", ValidType.NOT_LONG);
		//FunctionParamValid fpv = FunctionParamValid.create("orderId", ValidType.NOT_STR_DATETIME);
		valid(params, fpv);
		System.out.println("========");
		
	}

}
