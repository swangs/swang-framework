package com.swang.framework.core.constant;

/**
 * @Description: 数据类型定义
 * @ClassName: DataType.java
 * @Package:   com.yuntai.med.core.constant
 * @Author:    wangxiaoning@hsyuntai.com
 * @Date:      2016年1月26日 下午8:15:18
 * @Copyright: 版权归  恒生芸泰网络  所有
 */
public class DataType {
	/**Byte*/
	public static final int BYTE = 1; //
	/**Short*/
	public static final int SHORT = 2; //
	/**Integer*/
	public static final int INT = 3; //
	/**Long*/
	public static final int LONG = 4; //
	/**Float*/
	public static final int FLOAT = 5; //
	/**Double*/
	public static final int DOUBLE = 6; //
	/**Char*/
	public static final int CHAR = 7; //
	/**Boolean*/
	public static final int BOOLEAN = 8; //
	/**Date*/
	public static final int DATE = 9; //
	
	/**String*/
	public static final int STRING = 21; //  
	
	/**StringDate*/
	public static final int STR_DATE = 22; //
	/**StringDateTime*/
	public static final int STR_DATE_TIME = 23; 
	 

	public static void main(String[] args) {
		 

	}

}
