package com.swang.framework.core.mongodb;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by swang@swang.com on 2017/4/15.
 */
@Transactional
public interface LogInfoDao {

    void test();

    void createCollection();

    List<LogInfo> findList(int skip, int limit);


    LogInfo findOne(Long id);

    LogInfo findOneByBizIdType(Long bizId, String bizModule, Integer bizType);

    void insert(LogInfo logInfo);


}
