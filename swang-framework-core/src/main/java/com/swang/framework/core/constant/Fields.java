package com.swang.framework.core.constant;

public class Fields {

	/**
	 * 错误码
	 */
	public static final String ERROR_NO = "errorNo";

	/**
	 * 错误信息
	 */
	public static final String ERROR_INFO = "errorInfo";

	/**
	 * 医院Id
	 */

	public static final String HOS_ID = "hosId";
	/***
	 * 用户Id
	 */
	public static final String US_ID = "usId";
	/***
	 * 患者Id
	 */
	public static final String PAT_ID = "patId";
	/**
	 * 患者卡标识id
	 */
	public static final String PC_ID = "pcId";
	/**
	 * 支付渠道
	 */
	public static final String PAY_BY = "payBy";
	/**
	 * 预约记录Id
	 */
	public static final String BUSSINESS_ID = "bussinessId";

	/**
	 * 对接预约记录Id
	 */
	public static final String ACC_BIZ_ID = "accBizId";

	/**
	 * 订单Id
	 */
	public static final String ORDER_ID = "orderId";

	/**
	 * 微信用户标识
	 */
	public static final String OPEN_ID = "openid";

}
