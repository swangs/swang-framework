package com.swang.framework.core.enums;

/**
 * Created by swang@swang.com on 2017/4/15.
 */
public enum LogLevel {
    FATAL(5,"FATAL"),
    ERROR(4,"ERROR"),
    WARN(3,"WARN"),
    INFO(2,"INFO"),
    DEBUG(1,"DEBUG");

    public static  final  int FATAL_INT = 5;
    public static  final  int ERROR_INT = 4;
    public static  final  int WARN_INT = 3;
    public static  final  int INFO_INT = 2;
    public static  final  int DEBUG_INT = 1;
    private int value;
    private String name;
    private LogLevel(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
