package com.swang.framework.core.mongodb;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by swang@swang.com on 2017/4/15.
 */
public class LogInfo implements Serializable {
    //private  Long id; //主键自增ID
    private Long bizId;//业务数据ID表，记录业务表的主建ID
    private int bizType;//业务类型
    private String bizModule;//业务模块： pay-支付 , register-挂号 check-对账
    private String traceId;//追踪上下游调用的 id
    private String logInfo;//日志信内息容
    private String logData;//日志记录数据，JSON数据格式
    private int logLevel;//日志等级: 1-DEBUG, 2-INFO,3-WARN,4-ERROR,5-FATAL,默认为1
    private Date createTime;//创建时间，日志的生成时间

    public LogInfo(Long bizId, int bizType, String bizModule, String logInfo, String logData, int logLevel) {
        this(bizId, bizType, bizModule, "", logInfo, logData, logLevel, new Date());
    }

    public LogInfo(Long bizId, int bizType, String bizModule, String traceId, String logInfo, String logData, int logLevel, Date createTime) {
        this.bizId = bizId;
        this.bizType = bizType;
        this.bizModule = bizModule;
        this.traceId = traceId;
        this.logInfo = logInfo;
        this.logData = logData;
        this.logLevel = logLevel;
        if (createTime == null) {
            this.createTime = new Date();
        } else {
            this.createTime = createTime;
        }
    }

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public Long getBizId() {
        return bizId;
    }

    public void setBizId(Long bizId) {
        this.bizId = bizId;
    }

    public int getBizType() {
        return bizType;
    }

    public void setBizType(int bizType) {
        this.bizType = bizType;
    }

    public String getBizModule() {
        return bizModule;
    }

    public void setBizModule(String bizModule) {
        this.bizModule = bizModule;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getLogInfo() {
        return logInfo;
    }

    public void setLogInfo(String logInfo) {
        this.logInfo = logInfo;
    }

    public String getLogData() {
        return logData;
    }

    public void setLogData(String logData) {
        this.logData = logData;
    }

    public int getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(int logLevel) {
        this.logLevel = logLevel;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


}
