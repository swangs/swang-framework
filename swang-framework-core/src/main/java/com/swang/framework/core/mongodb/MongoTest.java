package com.swang.framework.core.mongodb;

import com.swang.framework.core.enums.LogLevel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by swang@swang.com on 2017/4/15.
 */
public class MongoTest
{
    private static Log log = LogFactory.getLog(MongoTest.class.getName());



    /**
     *<b>function:</b>main函数
     * @author cuiran
     * @createDate 2012-12-12 11:54:30
     */
    public static void main(String[] args) {
        System.out.println("Bootstrapping Mongo");

        ConfigurableApplicationContext context = null;
        context = new ClassPathXmlApplicationContext("applicationContext.xml");

        LogInfoDao infoDao = context.getBean(LogInfoDaoImpl.class);
        infoDao.createCollection();

        LogInfo log = null;
        for (int i = 0; i < 100000; i ++) {
            log = new LogInfo((long)i,1,"pms","这是一个测试" + i,"", LogLevel.DEBUG.getValue());
            infoDao.insert(log);
        }


        List<LogInfo> list = infoDao.findList(0, 10);
        for (LogInfo e : list) {
            System.out.println(e.getBizId());
        }

        System.out.println("DONE!");
    }
}
