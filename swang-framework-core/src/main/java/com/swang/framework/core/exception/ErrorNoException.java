package com.swang.framework.core.exception;

public class ErrorNoException extends MedException {
   
	private static final long serialVersionUID = 4759484959737989279L;
	
	private int errorNo = 0;
    public ErrorNoException(){
        super();
    }
    public ErrorNoException(String message){
    	super(message);
    }
 
    public ErrorNoException(int errorNo, String message){
    	super(message);
    	this.errorNo = errorNo;
    }

    public ErrorNoException(int errorNo, String message, Throwable cause){
    	super(message, cause);
    	this.errorNo = errorNo;
    }
    
	public int getErrorNo() {
		return errorNo;
	}
	
    @Override
    public String toString() {
        StringBuilder sb= new StringBuilder();
        sb.append("[").append((errorNo == 0) ? "" : ("errorNo:" + this.errorNo));
        sb.append(" message:").append(this.getMessage());
        sb.append(null == this.getCause()?"":( " caluse:" + this.getCause().toString()));
        sb.append("]");
        return sb.toString();
    }
}
