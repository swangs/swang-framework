package com.swang.framework.core.util;

import java.util.UUID;

/**
 * @ClassName: UUIDUtil
 * @Description: TODO
 * @author: swang@swang.com
 * @date: 2017/03/16 17:16
 */
public class UUIDUtil {
    public static String createUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
