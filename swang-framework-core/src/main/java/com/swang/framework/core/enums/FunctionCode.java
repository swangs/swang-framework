package com.swang.framework.core.enums;

/**
 * @Description: 接口功说明，增加功能号与接口功能名称
 * @ClassName: DataType.java
 * @Package:   com.yuntai.med.core.enums
 * @Author:    wangxiaoning@hsyuntai.com
 * @Date:      2016年1月26日 下午5:52:35
 * @Copyright: 版权归  恒生芸泰网络  所有
 */
public enum FunctionCode {
	 
	PAY_REGISTER_ORDER_CREATE("60082100","预约挂号订单创建/获取"),
	PAY_CLINIC_ORDER_CREATE("60082101","诊间支付订单创建/获取"),
	PAY_DIAGNOSIS_ORDER_CREATE("60082104","在线诊疗订单创建/获取"),
	PAY_DRUG_ORDER_CREATE("60082105","在线购药订单创建/获取"),
	PAY_ALIPAY_SIGN("60082200","支付宝签名"),
	PAY_ALIPAY_NOTIFY("60082201","支付宝支付异步通知入口"),
	PAY_ALIPAY_REFUND_NOTIFY("60082202","支付宝退款异步通知入口"),
	PAY_GET_CHANNEL("60082500","获取支付渠道"),
	PAY_HIS_SIGN("60082600","HIS平台支付宝支付"),
	PAY_HIS_NOTIFY("60082601","HIS平台支付宝支付异步通知入口"),
	PAY_PAT_PAYMENT("60082700","就诊卡支付"),
	PAY_TENPAY_SIGN("60082300","微信签名"),
	PAY_TENPAY_NOTIFY("60082301","微信支付异步通知入口"),
	PAY_YUNPAY_SIGN("60082400","云融惠付签名"),
	PAY_YUNPAY_NOTIFY("60082401","云融惠付支付异步通知入口");
	

	/**对外接名功能名称*/
	private String name;
	/**对外接口功能号： 目前规则是模块号+功能点*/
	private String code;

    private FunctionCode(String code, String name) {
        this.code = code;
        this.name = name; 
    }
   
    public String getCode() {
		return code;
	}
 

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
