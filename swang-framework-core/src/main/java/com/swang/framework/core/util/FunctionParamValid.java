package com.swang.framework.core.util;

import com.swang.framework.core.enums.ValidType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 功能参数验证
 * @ClassName: FunctionParamsValid.java
 * @Package:   com.yuntai.med.core.valid
 * @Author:    wangxiaoning@hsyuntai.com
 * @Date:      2016年1月27日 下午3:40:57
 * @Copyright: 版权归  恒生芸泰网络  所有
 */
public class FunctionParamValid {
	private String functionCode;
	private Map<String, ValidType> validParams = new HashMap<String,ValidType>();
	/**无功能号 默认值*/
	public static final String DEFAULT_FUNCTION_CODE="000000000";
	
	private FunctionParamValid(String functionCode) {
		this.functionCode = functionCode;
	}
	 
	private FunctionParamValid(String functionCode, String paramName, ValidType validType ) {
		this.functionCode = functionCode;
		validParams.put(paramName, validType);
	}
	public static	FunctionParamValid create(String functionCode) {
		return new FunctionParamValid(functionCode);
	}
	
	public static FunctionParamValid create(String paramName, ValidType validType ) {
		return new FunctionParamValid(DEFAULT_FUNCTION_CODE, paramName, validType);
	}
	public static	 FunctionParamValid create(String functionCode, String paramName, ValidType validType ) {
		return new FunctionParamValid(functionCode, paramName, validType);
	}
	
	public FunctionParamValid addValid(String paramName, ValidType validType) {
		validParams.put(paramName, validType);
		return this;
	}
	
	public FunctionParamValid removeValid(String paramName) {
		validParams.remove(paramName);
		return this;
	}
	
	public String getFunctionCode() {
		return functionCode;
	}

	public Map<String, ValidType> getValidParams() {
		return validParams;
	}
	
	public static final ValidType getValidType(FunctionParamValid fpv,String paramName) {
		if(fpv == null) {
			return null;
		} else {
			if(StringUtils.isNotBlank(paramName)) {
				return fpv.validParams.get(paramName);
			}
		}
		return null;
	}
	public ValidType getValidType(String paramName) {
		if(StringUtils.isNotBlank(paramName)) {
			return validParams.get(paramName);
		}
		return null;
	}
	public void clearValid() {
		validParams.clear();;
	}
	@Override
	public String toString() {
		 return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	} 
	public static void main(String[] args) {
		//创建一个valid如下
		FunctionParamValid fpv = FunctionParamValid.create("60082100").addValid("orderId", ValidType.NOT_NULL).addValid("bussinessId", ValidType.NOT_LONG);
		System.out.println("=======================");
		System.out.println(fpv);
	}

}
