package com.swang.framework.core.exception;

public class MedException extends RuntimeException {
 
	private static final long serialVersionUID = -7245546982447286614L;
    public MedException(){
        super();
    }

    public MedException(String message){
        super(message);
    }

    public MedException(String message, Throwable cause){
        super(message, cause);
    }
}
