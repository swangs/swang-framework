package com.swang.framework.core.constant;

/**
 * @Description:
 * @ClassName: ErrorCode.java
 * @Package: com.yuntai.med.core.util
 * @Author: wangxiaoning@hsyuntai.com
 * @Date: 2016年1月21日 11:49:46
 * @Copyright: 版权归 恒生芸泰网络 所有
 */
public class BaseErrorNo {

	/* 18.1 通用错误码（1XXX） */
	/**未指定{0}参数*/
	public static final int ERROR_GENERAL_1001 = 1001; // 未指定{0}参数
	/**参数{0}的值{1}无效*/
	public static final int ERROR_GENERAL_1002 = 1002; // 参数{0}的值{1}无效
	/**请求正文中不存在{0}对象或属性*/
	public static final int ERROR_GENERAL_1003 = 1003; // 请求正文中不存在{0}对象或属性
	/**查询字符串中的起始值大于或等于结束值*/
	public static final int ERROR_GENERAL_1004 = 1004; // 查询字符串中的起始值大于或等于结束值
	/**参数{0}重复出现*/
	public static final int ERROR_GENERAL_1005 = 1005; // 参数{0}重复出现
	/**无效的数据（{0}*/
	public static final int ERROR_GENERAL_1006 = 1006; // 无效的数据（{0}）
	/**无效区域识别码{0}*/
	public static final int ERROR_GENERAL_1007 = 1007; // 无效区域识别码{0}

	/* 18.2 身份鉴权（3XXX） */
	/**无效的U-Identity*/
	//public static final int ERROR_AUTH_3001 = 3001; // 无效的U-Identity
	/**客户端版本不一致*/
	//public static final int ERROR_AUTH_3002 = 3002; // 客户端版本不一致
	/**无效的UID（用户冻结或注销）*/
	public static final int ERROR_AUTH_3003 = 3003; // 无效的UID（用户冻结或注销）
	/**用户地理信息不存在*/
	//public static final int ERROR_AUTH_3004 = 3004; // 用户地理信息不存在
	/**新密码已生效，请重新登录*/
	public static final int ERROR_AUTH_3005 = 3005; // 新密码已生效，请重新登录
	/**该账号在其他地方登录*/
	public static final int ERROR_AUTH_3006 = 3006; // 该账号在其他地方登录
	/**该资源用户无权限访问*/
	public static final int ERROR_AUTH_3007 = 3007; // 该资源用户无权限访问
	/**Session已过期*/
	//public static final int ERROR_AUTH_3008 = 3008; // Session已过期
	/**无效的Token*/
	public static final int ERROR_AUTH_3009 = 3009; // 无效的Token
	/**Token已过期*/
	public static final int ERROR_AUTH_3010 = 3010; // Token已过期
	/**无效的PTOKEN*/
	public static final int ERROR_AUTH_3011 = 3011; // 无效的PTOKEN
	/**登录名或密码错误*/
	//public static final int ERROR_AUTH_3012 = 3012; // 登录名或密码错误
	/**Token即将要过期（12小时前预警）*/
	//public static final int ERROR_AUTH_3013 = "3013"; // Token即将要过期（12小时前预警）
	/** uid与token不匹配 */
	public static final int ERROR_AUTH_3014 = 3014; // uid与token不匹配
	/** 手机号发生变更，请重新登录 */
	public static final int ERROR_AUTH_3015 = 3015; // 手机号发生变更，请重新登录
	/** 您访问的频次过高 */
	public static final int ERROR_AUTH_3016 = 3016; // 您访问的频次过高
	/**内测阶段，暂停服务（不在接口白名单中）*/
	public static final int ERROR_AUTH_3017 = 3017; // 内测阶段，暂停服务（不在接口白名单中）

	/* 18.4 网络错误码（5XXX） */
	/**访问被拒绝*/
	public static final int ERROR_NET_5001 = 5001; // 访问被拒绝
	/** 网络异常或超时*/
	public static final int ERROR_NET_5002 = 5002; // 网络异常或超时
	/**网络繁忙*/
	public static final int ERROR_NET_5003 = 5003; // 网络繁忙
	/**单发消息频率超出最大限制{0} */
	public static final int ERROR_NET_5004 = 5004; // 单发消息频率超出最大限制{0}
	/**批量消息频率超出最大限制{0} */
	public static final int ERROR_NET_5005 = 5005; // 批量消息频率超出最大限制{0}

	/* 18.5 系统错误码（6XXX） */
	/**服务器当前处于只读模式*/
	public static final int ERROR_SYS_6001 = 6001; // 服务器当前处于只读模式
	/**服务器内部错误*/
	public static final int ERROR_SYS_6002 = 6002; // 服务器内部错误
	/**数据库发生异常*/
	public static final int ERROR_SYS_6003 = 6003; // 数据库发生异常
	/**系统繁忙*/
	public static final int ERROR_SYS_6004 = 6004; // 系统繁忙
	/**访问资源不存在*/
	public static final int ERROR_SYS_6005 = 6005; // 访问资源不存在
	/**请求格式错误 */
	public static final int ERROR_SYS_6006 = 6006; // 请求格式错误
	/**访问资源不存在*/
	public static final int ERROR_SYS_6007 = 6007; // 访问资源不存在
	
	//系统参数验证错误与编码 
	/**参数对象为 空 */
	public static final int ERROR_PARAM_7001 = 7001; //参数对象为 空 
	/**字串值不能为空*/
	public static final int ERROR_PARAM_7002 = 7002; // 字串值不能为空
	/**参数值不是有效的整数*/
	public static final int ERROR_PARAM_7003 = 7003; // 参数值不是有效的整数
	/**参数值不是有效的 浮点数*/
	public static final int ERROR_PARAM_7004 = 7004; // 参数值不是有效的浮点数
	/**参数值不是有效的布尔类型*/
	public static final int ERROR_PARAM_7005 = 7005; // 参数值不是有效的布尔类型
	/**参数【{%}={%}】值字串不是有效的日期与时间(yyyy-MM-dd)格式*/
	public static final int ERROR_PARAM_7006 = 7006; // 参数【{%}={%}】值字串不是有效的日期与时间(yyyy-MM-dd)格式
	/**访参数【{%}={%}】值字串不是有效的日期与时间(yyyy-MM-dd HH:mm:ss)格式*/
	public static final int ERROR_PARAM_7007 = 7007; //访参数【{%}={%}】值字串不是有效的日期与时间(yyyy-MM-dd HH:mm:ss)格式
	
	
}
