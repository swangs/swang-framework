package com.swang.framework.core.util;


import com.swang.framework.core.exception.ErrorNoException;

public class Assert {

	public static void isTrue(boolean expression, int errorNo, String message) {
		if (!expression) {
			throw new ErrorNoException(errorNo, message);
		}
	}

	public static void isTrue(boolean expression, String message) {
		if (!expression) {
			throw new IllegalArgumentException(message);
		}
	}
	public static void isTrue(boolean expression) {
		isTrue(expression, "[Assertion failed] - this expression must be true");
	}

	public static void isNull(Object object, int errorNo, String message) {
		if (object != null) {
			throw new ErrorNoException(errorNo, message);
		}
	}
	public static void isNull(Object object, String message) {
		if (object != null) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void isNull(Object object) {
		isNull(object, "[Assertion failed] - the object argument must be null");
	}

	public static void notNull(Object object, int errorNo, String message) {
		if (object == null) {
			throw new ErrorNoException(errorNo, message);
		}
	}

	public static void notNull(Object object, String message) {
		if (object == null) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void notNull(Object object) {
		notNull(object, "[Assertion failed] - this argument is required; it must not be null");
	}

	public static void notEmpty(Object object, int errorNo, String message) {
		if (ValueUtil.isEmpty(object)) {
			throw new ErrorNoException(errorNo, message);
		}
	}
	public static void notEmpty(Object object, String message) {
		if (ValueUtil.isEmpty(object)) {
			throw new IllegalArgumentException(message);
		}
	}
	public static void notEmpty(Object object) {
		notEmpty(object, "[Assertion failed] - this object must not be empty");
	}
	/**不为空且长度限制*/
	public static void notEmptyLength(Object object, int maxLen) {
		if (ValueUtil.isEmpty(object)) {
			throw new ErrorNoException("[Assertion failed] - this object must not be empty");
		} else {
			String str = object.toString();
			if(maxLen > 0 && str.length() > maxLen) {
				throw new ErrorNoException("[Assertion failed] - this object must < " + maxLen);
			}
		}
	}
	 public static void main(String[] args) {
		 long value = 1234567890;
		 notEmptyLength(value, 8);
	 }
}
